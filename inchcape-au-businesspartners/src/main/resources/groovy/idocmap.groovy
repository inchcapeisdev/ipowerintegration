import groovy.util.XmlSlurper

def ZBUPA_INBOUND_MAIN_SAVE01 = new XmlSlurper().parseText(flowVars.idoc)
def E101BUS_EI_CENTRAL_DATA = ZBUPA_INBOUND_MAIN_SAVE01.IDOC.E101BUS_EI_EXTERN.E101BUS_EI_CENTRAL_DATA
def addData = E101BUS_EI_CENTRAL_DATA.E101BUS_EI_BUPA_CENTRAL.E101US_EI_BUPA_CENTRAL_DATA.Z101GO_BUS_EI_STRUC_ADDDATA
def headerData = ZBUPA_INBOUND_MAIN_SAVE01.IDOC.E101BUS_EI_EXTERN.E101BUS_EI_HEADER
def personData = E101BUS_EI_CENTRAL_DATA.E101BUS_EI_BUPA_CENTRAL.E101US_EI_BUPA_CENTRAL_DATA.E101EI_STRUC_CENTRAL_PERSON
def organData = E101BUS_EI_CENTRAL_DATA.E101BUS_EI_BUPA_CENTRAL.E101US_EI_BUPA_CENTRAL_DATA.E101_EI_STRUC_CENTRAL_ORGAN
def addressData = E101BUS_EI_CENTRAL_DATA.E101BUS_EI_ADDRESS
def commData = E101BUS_EI_CENTRAL_DATA.E101BUS_EI_ADDRESS.E101BUS_EI_BUPA_ADDRESS.E101US_EI_BUPA_ADDRESS_DATA.E102BUS_EI_COMMUNICATION
def bpList = []
def sfInstance = [:]
def v360Instance = [:]





//declare transforms SAP -> SFDC
def titleMap = [
    "0001":"Ms.",
    "0002":"Mr.",
    "Z001":"Miss",
    "Z002":"Sir",
    "Z003":"Lady",
    "Z004":"Prof.",
    "Z005":"Reverend",
    "Z006":"Mrs.",
    "Z007":"Dr.",
    "Z008":"Mr & Mrs",
    "Z009":"Mr & Ms",
    "Z010":"Mr & Miss",
    "Z011":"Mrs & Mrs",
    "Z012":"Miss & Miss",
    "Z013":"Ms & Ms",
    "Z014":"Mr & Mr",
    "Z015":"Capt",
    "Z016":"Commissioner",
    "Z017":"Father",
    "Z018":"Hon",
    "Z019":"Lord",
    "Z020":"Prince",
    "Z021":"Princess",
    "Z022":"Rabbi",
    "Z023":"Senator",
    "Z024":"Sister"
]
def genderTransform = [
    "2":"Male",
    "1":"Female"
]
def countryTransform = [
    "AU":"Australia"
]
def stateTransform = [
    "NSW":"New South Wales",
    "VIC":"Victoria",
    "ACT":"Australian Capital Territory",
    "NT":"Northern Territory",
    "QLD":"Queensland",
    "SA":"South Australia",
    "TAS":"Tasmania",
    "WA":"Western Australia"
]

def privacyCodeMap = [
	"AC" : "PHONE / DIRECT MAIL",
	"EO": "EMAIL / PHONE / DIRECT MAIL",
	"ES" : "EMAIL / PHONE / SMS / DIRECT M",
	"NC" : "NO CAMPAIGNS / MARKETING",
	"SO" : "SMS / PHONE / DIRECT MAIL",	
	"EM" : "EMAIL/PHONE/SMS",
	"EN" : "EMAIL/PHONE/" ,
	"EP" : "EMAIL/DIRECT MAIL/SMS" ,
	"EQ" : "EMAIL/DIR.MAIL",
	"ET" : "EMAIL/SMS",
	"EU" : "EMAIL",
	"PS" : "PHONE/SMS",
	"PH" : "PHONE",
	"DS" : "DIRECT MAIL/SMS",
	"DM" : "DIRECT MAIL",
	"SM" : "SMS"
    
]
//Individual or Organisation? TODO: Family Support
if(E101BUS_EI_CENTRAL_DATA.E101BUS_EI_BUPA_CENTRAL.E101US_EI_BUPA_CENTRAL_DATA.E101US_EI_BUPA_CENTRAL_MAIN.CATEGORY == '1') {
    flowVars.entityType = "Individual"
    sfInstance.put("RecordTypeId",flowVars.privateRecordType)
    v360Instance.put("ContactType", "P")
    //sfInstance.put("Type","PRIVATE")


} else {
    flowVars.entityType = "Organisation"
    sfInstance.put("RecordTypeId",flowVars.businessRecordType)
    sfInstance.put("Type",addData.ZCONTACT_TYP.text())
    
    v360Instance.put("ContactType", "B") //insert some logic on ZCONTACT_TYP B, F, G
}
sfInstance.put("PrivacyCode__c", privacyCodeMap[addData.ZPRIVACY_CODE.text()])

//Entity Specific Data
if(flowVars.entityType == "Individual") {
    //salutation
    sfInstance.put("Salutation", titleMap[E101BUS_EI_CENTRAL_DATA.E101BUS_EI_BUPA_CENTRAL.E101US_EI_BUPA_CENTRAL_DATA.E101BUS_EI_STRUC_CENTRAL.TITLE_KEY.text()])
    if(personData.LASTNAME != '') {
        sfInstance.put("LastName",personData.LASTNAME.text())
        v360Instance.put("LastName",personData.LASTNAME.text())
    }
    if(personData.FIRSTNAME != '') {
        sfInstance.put("FirstName",personData.FIRSTNAME.text())
        v360Instance.put("FirstName",personData.FIRSTNAME.text())
    }
    if(personData.MIDDLENAME != '') {
        sfInstance.put("MiddleName",personData.MIDDLENAME.text())
       // v360Instance.put("FirstName",personData.MIDDLENAME.text())
    }
    if(personData.SEX != '') {
        sfInstance.put("Gender__c",genderTransform[personData.SEX.text()])
        v360Instance.put("Gender",genderTransform[personData.SEX.text()])
    }
    if(personData.BIRTHDATE != '00000000') {
        def date = new Date().parse("yyyyMMdd", personData.BIRTHDATE.text())
        sfInstance.put("Birthdate",date)
        v360Instance.put("DateOfBirth",date.format("yyyy-MM-dd"))
    }
    if(headerData.E101BUS_EI_INSTANCE.BPARTNER != '') {
        sfInstance.put("CustomerID__c",headerData.E101BUS_EI_INSTANCE.BPARTNER.text())
        //Sap BP ID is not stored in v360
    }

}

if(flowVars.entityType == "Organisation") {
    if(headerData.E101BUS_EI_INSTANCE.BPARTNER != '') {
        sfInstance.put("OrganisationID__c",headerData.E101BUS_EI_INSTANCE.BPARTNER.text())
        //Sap BP ID is not stored in v360
    }
    if(organData.NAME1 != '') {
        def fullName = organData.NAME1.text() + organData.NAME2.text() + organData.NAME3.text() + organData.NAME4.text()
        sfInstance.put("Name", fullName)
        v360Instance.put("EntityName", fullName)
    }
}

//Populate for ALL

def GROUPING = E101BUS_EI_CENTRAL_DATA.E101BUS_EI_BUPA_CENTRAL.E101US_EI_BUPA_CENTRAL_DATA.E101US_EI_BUPA_CENTRAL_MAIN.GROUPING
def PARTNEREXTERNAL = E101BUS_EI_CENTRAL_DATA.E101BUS_EI_BUPA_CENTRAL.E101US_EI_BUPA_CENTRAL_DATA.E101BUS_EI_STRUC_CENTRAL.PARTNEREXTERNAL

    if(GROUPING != '') {
        sfInstance.put("AccountGroup__c", GROUPING.text())
    }
    if(PARTNEREXTERNAL != '' && PARTNEREXTERNAL.text().isNumber()) {
        sfInstance.put("LegacyID__c", PARTNEREXTERNAL.text())
        v360Instance.put("EntityID", PARTNEREXTERNAL.text())
    } else {
        sfInstance.put("LegacyID__c",flowVars.legacyId.toString())
    }

//Address Data
addressData.E101BUS_EI_BUPA_ADDRESS.each {
    def ADDRESSTYPE = it.E101US_EI_BUPA_ADDRESS_DATA.E101BUS_EI_ADDRESSUSAGE.E101US_EI_BUPA_ADDRESSUSAGE.E101S_EI_STRUC_ADDR_USE_KEY.ADDRESSTYPE
    def STREET = it.E101US_EI_BUPA_ADDRESS_DATA.E101_EI_BUPA_POSTAL_ADDRESS.E101BUS_EI_STRUC_ADDRESS.STREET
    def STR_SUPPL1 = it.E101_EI_BUPA_POSTAL_ADDRESS.E101BUS_EI_STRUC_ADDRESS.STR_SUPPL1
    def HOUSE_NO = it.E101US_EI_BUPA_ADDRESS_DATA.E101_EI_BUPA_POSTAL_ADDRESS.E101BUS_EI_STRUC_ADDRESS.HOUSE_NO
    def CITY = it.E101US_EI_BUPA_ADDRESS_DATA.E101_EI_BUPA_POSTAL_ADDRESS.E101BUS_EI_STRUC_ADDRESS.CITY
    def REGION = it.E101US_EI_BUPA_ADDRESS_DATA.E101_EI_BUPA_POSTAL_ADDRESS.E101BUS_EI_STRUC_ADDRESS.REGION
    def POSTL_COD1 = it.E101US_EI_BUPA_ADDRESS_DATA.E101_EI_BUPA_POSTAL_ADDRESS.E101BUS_EI_STRUC_ADDRESS.POSTL_COD1
    def COUNTRY = it.E101US_EI_BUPA_ADDRESS_DATA.E101_EI_BUPA_POSTAL_ADDRESS.E101BUS_EI_STRUC_ADDRESS.COUNTRY

    //Address Types: 0001 - Correspondance, 0002 - Home, 0003 - Business

    //Define default address for Organisation / Individual if no code provided
    println(ADDRESSTYPE)
    if(flowVars.entityType == "Individual" && ADDRESSTYPE == "XXDEFAULT") {
        sfInstance.put("OtherStreet", STREET.text() + STR_SUPPL1.text())
        sfInstance.put("OtherCity", CITY.text())
        sfInstance.put("OtherState",stateTransform[REGION.text()])
        sfInstance.put("OtherPostalCode", POSTL_COD1.text())
        sfInstance.put("OtherCountry", countryTransform[COUNTRY.text()])
        sfInstance.put("ResidentialDPID__c", HOUSE_NO.text())

        v360Instance.put("Street", STREET.text() + STR_SUPPL1.text())
        v360Instance.put("City", CITY.text())
        v360Instance.put("State", REGION.text())
        v360Instance.put("PostalCode", POSTL_COD1.text())
        //No Country in API yet.
    }
    if(flowVars.entityType == "Organisation" && ADDRESSTYPE == "XXDEFAULT") {
        sfInstance.put("BillingStreet", STREET.text() + STR_SUPPL1.text())
        sfInstance.put("BillingCity", CITY.text())
        sfInstance.put("BillingState", stateTransform[REGION.text()])
        sfInstance.put("BillingPostalCode", POSTL_COD1.text())
        sfInstance.put("BillingCountry",countryTransform[COUNTRY.text()])
       // sfInstance.put("BillingDPID__c",it.E101US_EI_BUPA_ADDRESS_DATA.E101_EI_BUPA_POSTAL_ADDRESS.E101BUS_EI_STRUC_ADDRESS.HOUSE_NO.text()) Field needs adding to SF

       v360Instance.put("BusAddressLine1", STREET.text() + STR_SUPPL1.text())
       v360Instance.put("BusSuburb", CITY.text())
       v360Instance.put("BusState", REGION.text())
       v360Instance.put("BusPostCode", POSTL_COD1.text())
       //No Country in API yet.
    }

    if((ADDRESSTYPE.toString().contains("0002")) && flowVars.entityType == "Individual") {
        sfInstance.put("OtherStreet", STREET.text() + STR_SUPPL1.text())
        sfInstance.put("OtherCity", CITY.text())
        sfInstance.put("OtherState",stateTransform[REGION.text()])
        sfInstance.put("OtherPostalCode", POSTL_COD1.text())
        sfInstance.put("OtherCountry", countryTransform[COUNTRY.text()])
        sfInstance.put("ResidentialDPID__c", HOUSE_NO.text())

        v360Instance.put("OtherStreet", STREET.text() + STR_SUPPL1.text())
        v360Instance.put("OtherCity", CITY.text())
        v360Instance.put("OtherState",REGION.text())
        v360Instance.put("OtherPostalCode", POSTL_COD1.text())
        //No Country in API yet.
    }
    if((ADDRESSTYPE.toString().contains("0001")) && flowVars.entityType == "Individual") {
        sfInstance.put("MailingStreet", STREET.text() + STR_SUPPL1.text())
        sfInstance.put("MailingCity", CITY.text())
        sfInstance.put("MailingState", stateTransform[REGION.text()])
        sfInstance.put("MailingPostalCode", POSTL_COD1.text())
        sfInstance.put("MailingCountry", countryTransform[COUNTRY.text()])
        sfInstance.put("MailingDPID__c", HOUSE_NO.text())

        v360Instance.put("PosAddressLine1", STREET.text() + it.E101_EI_BUPA_POSTAL_ADDRESS.E101BUS_EI_STRUC_ADDRESS.STR_SUPPL1.text())
        v360Instance.put("PosSuburb", CITY.text())
        v360Instance.put("PosState",REGION.text())
        v360Instance.put("PosPostCode", POSTL_COD1.text())
        //No Country in API yet.
    }
    if((ADDRESSTYPE.toString().contains("SHIP_TO")) && flowVars.entityType == "Organisation") {
        sfInstance.put("ShippingStreet", STREET.text() + STR_SUPPL1.text())
        sfInstance.put("ShippingCity", CITY.text())
        sfInstance.put("ShippingState",stateTransform[REGION.text()])
        sfInstance.put("ShippingPostalCode", POSTL_COD1.text())
        sfInstance.put("ShippingCountry",countryTransform[COUNTRY.text()])
        //sfInstance.put("ShippingDPID__c",it.E101US_EI_BUPA_ADDRESS_DATA.E101_EI_BUPA_POSTAL_ADDRESS.E101BUS_EI_STRUC_ADDRESS.HOUSE_NO.text()) field needs adding to SF
    }
    if((ADDRESSTYPE.toString().contains("0003")) && flowVars.entityType == "Organisation") {
        sfInstance.put("BillingStreet", STREET.text() + STR_SUPPL1.text())
        sfInstance.put("BillingCity", CITY.text())
        sfInstance.put("BillingState",stateTransform[REGION.text()])
        sfInstance.put("BillingPostalCode", POSTL_COD1.text())
        sfInstance.put("BillingCountry",countryTransform[COUNTRY.text()])
       // sfInstance.put("BillingDPID__c",it.E101US_EI_BUPA_ADDRESS_DATA.E101_EI_BUPA_POSTAL_ADDRESS.E101BUS_EI_STRUC_ADDRESS.HOUSE_NO.text()) field needs adding to SF

       v360Instance.put("BusAddressLine1", STREET.text() + STR_SUPPL1.text())
       v360Instance.put("BusSuburb", CITY.text())
       v360Instance.put("BusState",REGION.text())
       v360Instance.put("BusPostCode", POSTL_COD1.text())
       //No Country in API yet.
    }
}

//Communication Data
commData.E102BUS_EI_TEL_DATA.E102BUS_EI_BUPA_TELEPHONE.each {
    def COMM_NOTES = it.E113BUS_EI_COMREM.E113BUS_EI_BUPA_COMREM.E113BUS_EI_STRUC_COMREM.COMM_NOTES
    def TELEPHONE = it.E102S_EI_BUPA_TELEPHONE_CON.E102BUS_EI_STRUC_TEL_DATA.TELEPHONE
    //println("Processing Phone: " + it.E113BUS_EI_COMREM.E113BUS_EI_BUPA_COMREM.E113BUS_EI_STRUC_COMREM.COMM_NOTES)
    if(COMM_NOTES == "Home" && flowVars.entityType == "Individual") {
            sfInstance.put("Phone", TELEPHONE.text())
            v360Instance.put("PhoneHome", TELEPHONE.text())
    }
    if(COMM_NOTES == "Work") {
            sfInstance.put("WorkPhone__c", TELEPHONE.text())
            v360Instance.put("PhoneBusiness", TELEPHONE.text())
    }
    if(COMM_NOTES == "Mobile" && flowVars.entityType == "Individual") {
            sfInstance.put("MobilePhone", TELEPHONE.text())
            v360Instance.put("PhoneMobile", TELEPHONE.text())
    }
    if(COMM_NOTES == "Mobile" && flowVars.entityType == "Organisation") {
            sfInstance.put("Mobile__c", TELEPHONE.text())
            v360Instance.put("PhoneMobile", TELEPHONE.text())
    }
}

commData.E102BUS_EI_FAX_DATA.E102BUS_EI_BUPA_FAX.each {
    if(it.E102BUS_EI_BUPA_FAX_CON.E102BUS_EI_STRUC_FAX_DATA.FAX != null) {
        sfInstance.put("Fax", it.E102BUS_EI_BUPA_FAX_CON.E102BUS_EI_STRUC_FAX_DATA.FAX.text())
    }
}

commData.E102BUS_EI_SMTP_DATA.E102BUS_EI_BUPA_SMTP.each {
    //println("Processing Email: " + it.E117BUS_EI_COMREM.E117BUS_EI_BUPA_COMREM.E117BUS_EI_STRUC_COMREM.COMM_NOTES)
    def COMM_NOTES = it.E117BUS_EI_COMREM.E117BUS_EI_BUPA_COMREM.E117BUS_EI_STRUC_COMREM.COMM_NOTES
    def E_MAIL = it.E102BUS_EI_BUPA_SMTP_CON.E102BUS_EI_STRUC_SMTP_DATA.E_MAIL

    if((COMM_NOTES == "Home" || COMM_NOTES == "HomeXXDEFAULT" || COMM_NOTES == "XXDEFAULTHome") && flowVars.entityType == "Individual") {
        sfInstance.put("Email", E_MAIL.text())
        v360Instance.put("EmailHome", E_MAIL.text())
    }
    if((COMM_NOTES == "Work" || COMM_NOTES == "WorkXXDEFAULT" || COMM_NOTES == "XXDEFAULTWork")) {
        sfInstance.put("BusinessEmail__c", E_MAIL.text())
        v360Instance.put("EmailBusiness", E_MAIL.text()) 
    }
}

flowVars.sapReturnData = sfInstance
flowVars.v360Entity = v360Instance
bpList.push(sfInstance)

flowVars.sfEntity = bpList
//message.payload = bpList
