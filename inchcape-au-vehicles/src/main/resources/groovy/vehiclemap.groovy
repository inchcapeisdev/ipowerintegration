import groovy.util.XmlSlurper

//evaluate(new File("../transforms.groovy"))

def ZFM_GO_DBM_VEHICLE_GET_LIST = new XmlSlurper().parseText(message.payload)
def ET_IOBJ_DATA_MULTI_COM = ZFM_GO_DBM_VEHICLE_GET_LIST.export.ET_IOBJ_DATA_MULTI_COM
def ET_IOBJ_DATA_SINGLE_COM = ZFM_GO_DBM_VEHICLE_GET_LIST.export.ET_IOBJ_DATA_SINGLE_COM
def ET_VLCDIAVEHI = ZFM_GO_DBM_VEHICLE_GET_LIST.export.ET_VLCDIAVEHI
def ET_SRV_HIST = ZFM_GO_DBM_VEHICLE_GET_LIST.export.ET_SRV_HIST
def DBM_V_IMODELT = ZFM_GO_DBM_VEHICLE_GET_LIST.export.DBM_V_IMODELT

def vehicleList = []
def vehicleInstance = [:]

def srvHistInstance = [:]
def srvHistList = []
def optionInstance = [:]
def optionList = []
def recallList = []
def recallInstance = [:]

def vehicleType = flowVars.flowName
//def vehicleType = "Owned"

def transactionMap = [
    "NEC":"New",
    "USC":"Used"
]
def fuelMap = [
    "PL":"Petrol"
    //Diesel?
]
def transmissionMap = [
    "T01":"Manual",
    "T02":"Automatic",
    "T03":"Tiptronic",
    "T04":"Multimode"
]
def boolMap = [
    "X":true,
    "":false
]

/*def modelLineMap = [
    "WRX":"WRX",
    "WRX STI":"WRX STI",
    "LIBERTY":"Liberty",
    "XV":"XV",
    "FORESTER":"Forester",
    "IMPREZA":"Impreza",
    "TRIBECA":"Tribeca",
    "BRZ":"BRZ",
    "OUTBACK":"Outback" 
] <- UNUSED */

def bodyTypeMap = [
    "CAB":"Convertible",
    "CBL":"Cabriolet",
    "COP":"Coupe",
    "EST":"Estate",
    "FSB":"Fastback",
    "HBK":"Hatchback",
    "LBK":"Liftback",
    "MPV":"MPV",
    "ORD":"Offroad",
    "PKP":"Pickup",
    "SALN":"Saloon",
    "SDN":"Sedan",
    "TPR":"Tipper",
    "VN":"Van"
]
def rtaBodyTypeMap = [
    "CON":"Convertible",
    "COU":"Coupe",
    "CYC":"Motor Cycle",
    "CYS":"Motor Cycle With Side Car",
    "FCV":"Forward Control Passenger Vehicle",
    "MH":"Mobile Home",
    "PVF":"4 Wheel Drive Panel Van with Seats & Windows",
    "PVN":"Panel Van",
    "PVS":"2 Wheel Drive Panel Van with Seats & Windows",
    "PVW":"Panel Van with Side Windows",
    "SBS":"Small Bus Seating up to 9 persons incl. Driver",
    "SED":"Sedan",
    "SNO":"Snow Vehicle",
    "TWC":"Three Wheel Car",
    "UTE":"Utility",
    "VAN":"Van",
    "WAG":"Station Wagon"
]

def purchaseTypeMap = [
    "P01":"Trade in",
    "P02":"Direct Purchase",
    "P03":"Commission",
    "P04":"Broker",
    "P05":"Outside Group",
    "P06":"Within Group",
    "P07":"Central Purchasing",
    "P08":"OEM",
    "P09":"Trade in/Commission"
]
def sectorTypeMap = [
    "Z001":"New",
    "Z002":"Used",
    "Z003":"New",
    "Z004":"Loan",
    "Z007":"Demo",
    "Z008":"Demo",
    "Z009":"Demo",
    "Z013":"Demo",
    "Z015":"Demo",
    "Z018":"Demo",
    "Z019":"Demo",
    "Z020":"Demo",
    "Z021":"Demo",
    "Z022":"Demo",
    "Z023":"Demo",
    "Z024":"Demo",
    "Z025":"New",
    "Z026":"New",
    "Z027":"",
    "Z028":"Demo",
    "Z029":"Demo"
]
def sectorMap = [
    "Z001":"NEW VEHICLE STOCK",
    "Z002":"USED VEHICLE STOCK",
    "Z003":"NEW PRE-REGISTERED STOCK",
    "Z004":"RENTAL/COURTESY CAR - SERVICE",
    "Z005":"RENTAL/COURTESY CAR - BODYSHOP",
    "Z006":"RENTAL/COURTESY CAR - VEHICLE RENTAL",
    "Z007":"DEMO - USED BY NEW DEPARTMENT",
    "Z008":"DEMO - USED BY USED DEPARTMENT",
    "Z009":"DEMO - USED BY NEW FLEET DEPARTMENT",
    "Z010":"DEMO - USED BY NEW AGENCY DEPARTMENT",
    "Z011":"DEMO - USED BY NEW INTERNET/OTHER DEPARTMENT",
    "Z012":"DEMO - USED BY USED INTERNET/OTHER DEPARTMENT",
    "Z013":"DEMO - USED BY SERVICE DEPARTMENT",
    "Z014":"DEMO - USED BY BODYSHOP DEPARTMENT",
    "Z015":"DEMO - USED BY PARTS DEPARTMENT",
    "Z016":"DEMO - USED BY VEHICLE RENTAL DEPARTMENT",
    "Z017":"DEMO - USED BY FUEL FORECOURT DEPARTMENT",
    "Z018":"DEMO - USED BY OTHER RETAIL CENTRE DEPARTMENT",
    "Z019":"DEMO - USED BY RETAIL CENTRE OVERHEADS",
    "Z020":"DEMO - USED BY FINANCE & ACCOUNTING",
    "Z021":"DEMO - USED BY HUMAN RESOURCES",
    "Z022":"DEMO - USED BY TRAINING DEPARTMENT",
    "Z023":"DEMO - USED BY PROPERTY DEPARTMENT",
    "Z024":"DEMO - USED BY INFORMATION SYSTEMS",
    "Z025":"NEW INVOICED - PAID STOCK",
    "Z026":"PDI - CONSIGNMENT VEHICLES",
    "Z027":"Service Vehicle"
]
def stockStatusMap = [
    "ZP27:ZS05":"In Stock",
    "ZP40:ZS05":"In Stock",
    "ZP70:ZS05":"In Stock",
    "ZP27:ZS05":"Loan Car",
    "ZP40:ZS05":"Loan Car",
    "ZP60:ZS05":"Loan Car",
    "ZP70:ZS05":"Loan Car",
    "ZP20:ZS05":"On Order",
    "ZP30:ZS05":"On Order",
    "ZP27:ZS05":"Demo",
    "ZP40:ZS05":"Demo",
    "ZP60:ZS05":"Demo",
    "ZP70:ZS05":"Demo"
]

def locationMap = [
    "L054":"Holding Pool",
    "L055":"Awaiting Approval",
    "L056":"Order Created",
    "L057":"Order Confirmed",
    "L058":"Submitted",
    "L059":"Order Committed",
    "L060":"Order Being Built",
    "L061":"Order Built",
    "L062":"Accepted by Sales",
    "L063":"Despatched",
    "L064":"Into Port of Exit or Local Compound",
    "L065":"SHIPPED",
    "L066":"Into Port of Entry or Receiving Compound",
    "L067":"In Transit to Dealer",
    "L068":"Arrived at Dealer",
    "L069":"Customer Handover",
    "L070":"Order Cancelled",
    "L071":"Vehicle in transit",
    "L072":"Transferred to another dealer",
    "L073":"Orders placed",
    "L074":"on the conveyor",
    "L075":"Zafiks in Manufact",
    "L076":"Shipped to the port",
    "L077":"built",
    "L078":"Shipped to Russia",
    "L079":"Shipped with PLP",
    "L080":"Cleared (D)",
    "L081":"Cleared (PLP)",
    "L082":"For shipment c PLP",
    "L083":"MMVO Magistalnaya",
    "L084":"For shipment from Domod",
    "L085":"MMVO Varshavskoe",
    "L086":"UM 29 km MKAD",
    "L087":"Build-up vehicles CBU",
    "L088":"Build-up vehicles CKD",
    "L089":"Vehicle left the factory",
    "L090":"Vehicle on stock BMW RUS",
    "L091":"Shipping from BRT",
    "L092":"BH 29 km MKAD",
    "L093":"BH Yaroslavskoe",
    "L094":"BH Balashiha",
    "L095":"BH Olimpisky",
    "L096":"RR Kutuzovskiy",
    "L097":"RR Malaya Morskaya",
    "L099":"Plant Goodwood",
    "L100":"Transportation",
    "L101":"Penrith - On Site",
    "L102":"Denlo - On Site",
    "L103":"Narellan - On Site",
    "L104":"North Shore - On Site",
    "L105":"Waitara - On Site",
    "L106":"Sydney Retail Company Cars",
    "L107":"Sydney Corporate - On Site",
    "L108":"Denlo - PK Comp",
    "L109":"Denlo - Rosehill Comp",
    "L110":"Denlo - Vic Comp",
    "L111":"Denlo - Qld Comp",
    "L112":"Denlo - WA Comp",
    "L113":"Penrith - PK Comp",
    "L114":"Penrith - Rosehill Comp",
    "L115":"Penrith - Vic Comp",
    "L116":"Penrith - Qld Comp",
    "L117":"Penrith - WA Comp",
    "L118":"Narellan - PK Comp",
    "L119":"Narellan - Rosehill Comp",
    "L120":"Narellan - Vic Comp",
    "L121":"Narellan - Qld Comp",
    "L123":"Narellan - WA Comp",
    "L124":"North Shore - PK Comp",
    "L125":"North Shore - Rosehill Comp",
    "L126":"North Shore - Vic Comp",
    "L127":"North Shore - Qld Comp",
    "L128":"North Shore - WA Comp",
    "L129":"Waitara - PK Comp",
    "L130":"Waitara - Rosehill Comp",
    "L131":"Waitara - Vic Comp",
    "L132":"Waitara - Qld Comp",
    "L133":"Waitara - WA Comp",
    "L134":"Denlo - SA Comp",
    "L135":"Penrith - SA Comp",
    "L136":"Narellan - SA Comp",
    "L137":"North Shore - SA Comp",
    "L138":"Waitara - SA Comp"
]

def MMSTA = [:]
def SDSTA = [:]

ET_VLCDIAVEHI.row.each {
    //Global VH Lookup
    vehicleInstance.put("VGUID", it.VGUID.text())
    vehicleInstance.put("IOBJGUID", it.IOBJGUID.text())
    //Stock specific Mapping
    MMSTA.put(it.VGUID.text(),it.MMSTA.text())
    SDSTA.put(it.VGUID.text(),it.SDSTA.text())
    
    //if(vehicleType == "Stock") {
    //    def statusKey = it.MMSTA.text() + ":" + it.SDSTA.text()
    //    vehicleInstance.put("StockStatus__c", convertStockMapping(it.MMSTA.text(),it.SDSTA.text(),))
    //}
    //Stock/Owned shared mappings
    if(it.ZZVEH_KEY != '') {
        vehicleInstance.put("ZZVEH_KEY", it.ZZVEH_KEY.text())
    }
    if(it.PCOUNT != '') {
        vehicleInstance.put("LastMileage__c", it.PCOUNT.text())
    }
    if(it.DBM_LICEXT != '') {
        vehicleInstance.put("LicencePlate__c", it.DBM_LICEXT.text())
    }
    if(it.VHCLE != '') {
        vehicleInstance.put("StockNumber__c", it.VHCLE.text())
    }
    if(it.VHCEX != '') {
        vehicleInstance.put("LegacyID__c", it.VHCEX.text())
    }
    if(it.VHVIN != '') {
        vehicleInstance.put("VIN__c", it.VHVIN.text())
    }
    if(it.DBM_BUSTYPE != '') {
        vehicleInstance.put("BusinessTransactionType__c", transactionMap[it.DBM_BUSTYPE.text()])
    }
    //if(it.KUNNR != '') {
    //SF Id, of any vehicle owner, appears as a SAP BP Id, requires a query to SF to get ID.
    //    vehicleInstance.put("KUNNR", it.KUNNR.text())
    //}

    vehicleList.push(vehicleInstance)
    vehicleInstance = [:]
}

ET_IOBJ_DATA_MULTI_COM.row.each {
    def vguid = it.'_-DBM_-V_VEHICLE'.VGUID.text()
    def listElement = vehicleList.find { it.VGUID == vguid}
    
    //Stock/Owned shared mappings
    listElement.put("ModelDescription__c",it.DBM_V_IMODELT.row.TEXT2.text())
    listElement.put("Carline__c",it.DBM_V_IMODELT.row.TEXT1.text())
    
    if(vehicleType == 'Stock') {
        it.DBM_V_IOPTION.row.each {
            optionInstance.put("PRODUCT_GUID", it.PRODUCT_GUID.text())
            optionInstance.put("FeatureKey__c", it.OPKEY.text())
            optionInstance.put("FeatureCategory__c", it.OPTYP.text())
            optionInstance.put("FeatureClassification__c", it.OPCLASS.text())
            optionInstance.put("FeatureSellingPrice__c", it.SAPRC.text())
            optionInstance.put("FeaturePurchasePrice__c", it.PUPRC.text())
            optionList.push(optionInstance)
            optionInstance = [:]
        }
        it.DBM_V_IOPTIONT.row.each {
            def opkey = it.OPKEY.text()
            def optionElement = optionList.find { it.FeatureKey__c == opkey}
            //def optionText = ""
           // optionList << it.OPKEY.text()
            optionElement.put("FeatureDescription__c", it.OPTEXT1.text())
               // listElement.put("VehicleFeatures__c", optionString)

        }
        listElement.put("OPTIONS", optionList)
        println(optionList)
        optionList = []
    }
    if(vehicleType == 'Owned') {
        it.DBM_V_IPARTNER.row.each {
            def objguid = it.PRODUCT_GUID.text()
            def vehicleElement = vehicleList.find { it.IOBJGUID == objguid}
            if(it.PARVW == "OW"  || it.PARVW == "ZO" ) {
                vehicleElement.put("CurrentOwner__c", it.KUNNR.text())
            }
            if(it.PARVW == "DR" || it.PARVW == "ZD" ) {
                vehicleElement.put("Driver__c", it.KUNNR.text())
            }
            if(it.PARVW == "RG" || it.PARVW == "PY") {
                vehicleElement.put("Payer__c", it.KUNNR.text())
            }
            println(vehicleElement)
        }
    }
    it.DBM_V_IRCL.row.each {
        //def objguid = it.PRODUCT_GUID.text()
        //def vehicleElement = vehicleList.find { it.IOBJGUID == objguid}
        
        recallInstance.put("RecallNumber__c", it.RCLMNO.text())
        recallInstance.put("GUID__c", it.'_-DBM_-GUID'.text())
        recallInstance.put("VersionNumber__c", it.VERSN.text())
        recallInstance.put("DescriptionOfOperation__c", it.RCLMNO_DESC.text())
        recallInstance.put("Status__c", it.RCLSTAT_DESC.text())
        //map in status
        recallList.push(recallInstance)
        recallInstance = [:]
    }
    listElement.put("RECALL", recallList)
    recallList = []
}
    

ET_IOBJ_DATA_SINGLE_COM.row.each {
    def vguid = it.'_-DBM_-V_VEHICLE'.VGUID.text()
    def listElement = vehicleList.find { it.VGUID == vguid}

    //Stock/Owned shared mappings
        //Store the code for SF lookup
        listElement.put("MCODESD",it.DBM_V_IMODEL.MCODESD.text())
    if(it.ZZDBM_V_MODEL.ZZ0030 != '') {
        listElement.put("RTABodyType__c", rtaBodyTypeMap[it.ZZDBM_V_MODEL.ZZ0030.text()])
    }
    if(it.DBM_V_IMODEL.BODTYPE != '') {
        listElement.put("BodyStyle__c", bodyTypeMap[it.DBM_V_IMODEL.BODTYPE.text()])
    }    
    if(it.DBM_V_IMODEL.CONYEAR != '0000') {
        listElement.put("YearManufactured__c", it.DBM_V_IMODEL.CONYEAR.text()) 
    }
    if(it.DBM_V_IMODEL.MODYEAR != '') {
        listElement.put("ModelYear__c", it.DBM_V_IMODEL.MODYEAR.text())
    }
    if(it.DBM_V_IMODEL.ENG_FUEL != '') {
        listElement.put("FuelType__c", fuelMap[it.DBM_V_IMODEL.ENG_FUEL.text()])
    }
    if(it.DBM_V_IMODEL.CUBIC_CAP != '') {
        listElement.put("CubicCapacity__c", it.DBM_V_IMODEL.CUBIC_CAP.text())
    }    
    if(it.ZZDBM_V_MODEL.ZZ0031 != '') {
        listElement.put("Make__c", it.ZZDBM_V_MODEL.ZZ0031.text())
    }
    if(it.ZZDBM_V_MODEL.ZZ0024 != '') {
        listElement.put("FleetDiscount__c", it.ZZDBM_V_MODEL.ZZ0024.text())
    }
    if(it.ZZDBM_V_MODEL.ZZ0027 != '') {
        listElement.put("GovernmentDiscount__c", it.ZZDBM_V_MODEL.ZZ0027.text())
    }
    //perform model lookup logic

    if(it.ZZDBM_V_MODEL.ZZ0020 != '') {
        listElement.put("Model__c", it.ZZDBM_V_MODEL.ZZ0020.text())
    }
    if(it.DBM_V_IVEHICLE.ENGCODE != '') {
        listElement.put("EngineNumber__c",it.DBM_V_IVEHICLE.ENGCODE.text())
    }
    if(it.ZZDBM_V_GEN.ZZ0039 != '') {
        listElement.put("BaseColour__c",it.ZZDBM_V_GEN.ZZ0039.text())
    }    
    if(it.ZZDBM_V_GEN.ZZ0041 != '') {
        listElement.put("ExteriorColour__c",it.ZZDBM_V_GEN.ZZ0041.text())
    }  
    if(it.ZZDBM_V_GEN.ZZ0043 != '') {
        listElement.put("InteriorColour__c",it.ZZDBM_V_GEN.ZZ0043.text())
    }    
    if(it.ZZDBM_V_GEN.ZZ0047 != '') {
        listElement.put("TransmissionNumber__c",transmissionMap[it.ZZDBM_V_GEN.ZZ0047.text()])
    }
    if(it.DBM_V_IMODEL.NODOORS != '') {
        listElement.put("NumberOfDoors__c", it.DBM_V_IMODEL.NODOORS.text())
    }    
    if(it.DBM_V_IVEHICLE.LABVAL_TY != '') {
        listElement.put("LVMainType__c",it.DBM_V_IVEHICLE.LABVAL_TY.text())
    }    
    if(it.ZZDBM_V_GEN.ZZ0028 != '0000-00-00') {
        def date = new Date().parse("yyyy-MM-dd", it.ZZDBM_V_GEN.ZZ0028.text()).format("yyyy-MM-dd")
        listElement.put("LastWorkedDate__c",date + "T10:00:00Z")
    }
    if(it.ZZDBM_V_GEN.ZZ0029 != '0000-00-00') {
        def date = new Date().parse("yyyy-MM-dd", it.ZZDBM_V_GEN.ZZ0029.text()).format("yyyy-MM-dd")
        listElement.put("LastServiceDate__c",date + "T10:00:00Z")
    }
    if(it.ZZDBM_V_GEN.ZZ0031 != '0000-00-00') {
        def date = new Date().parse("yyyy-MM-dd", it.ZZDBM_V_GEN.ZZ0031.text()).format("yyyy-MM-dd")
        listElement.put("NextServiceDate__c",date + "T10:00:00Z")
    }       
    
    //Stock specific mapping
    if(vehicleType == 'Stock') {
        if(it.ZZDBM_V_VMSGEN.ZZ0023 != '') {
            //perform status transform
            def MMSTAElement = MMSTA.find { key, value -> key == vguid}
            def SDSTAElement = SDSTA.find { key, value -> key == vguid}
            listElement.put("StockStatus__c", convertStockMapping(MMSTAElement.value, SDSTAElement.value, it.ZZDBM_V_VMSGEN.ZZ0023.text()))

            listElement.put("Sector__c", sectorMap[it.ZZDBM_V_VMSGEN.ZZ0023.text()])
            listElement.put("Type__c", sectorTypeMap[it.ZZDBM_V_VMSGEN.ZZ0023.text()])
        }
        if(it.ZZDBM_V_VMSGEN.ZZ0025 != '') {
            listElement.put("Location__c", locationMap[it.ZZDBM_V_VMSGEN.ZZ0025.text()])
        }
        if(it.ZZDBM_V_AU_GEN.ZZ0013 != '') {
            listElement.put("Memo1__c", it.ZZDBM_V_AU_GEN.ZZ0013.text())
        }
        if(it.ZZDBM_V_AU_GEN.ZZ0014 != '') {
            listElement.put("Memo2__c", it.ZZDBM_V_AU_GEN.ZZ0014.text())
        }    
        if(it.ZZDBM_V_GEN.ZZ0054 != '') {
            listElement.put("PurchaseType__c",purchaseTypeMap[it.ZZDBM_V_GEN.ZZ0054.text()])
        }
        if(it.ZZDBM_V_VMSGEN.ZZ0010 != '0000-00-00') {
            def date = new Date().parse("yyyy-MM-dd", it.ZZDBM_V_VMSGEN.ZZ0010.text()).format("yyyy-MM-dd")
            listElement.put("DateInDealerStock__c", date + "T10:00:00Z")
        }
        if(it.ZZDBM_V_VMSGEN.ZZ0012 != '0000-00-00') {
            def date = new Date().parse("yyyy-MM-dd", it.ZZDBM_V_VMSGEN.ZZ0012.text()).format("yyyy-MM-dd")
            listElement.put("ExpectedArrivalDate__c", date + "T10:00:00Z")
        }
        if(it.ZZDBM_V_VMSGEN.ZZ0030 != '0000-00-00') {
            def date = new Date().parse("yyyy-MM-dd", it.ZZDBM_V_VMSGEN.ZZ0030.text()).format("yyyy-MM-dd")
            listElement.put("ActualArrivalDate__c", date + "T10:00:00Z")
        }
        if(it.ZZDBM_V_VMSGEN.ZZ0016 != '0000-00-00') {
            def date = new Date().parse("yyyy-MM-dd", it.ZZDBM_V_VMSGEN.ZZ0016.text()).format("yyyy-MM-dd")
            listElement.put("BuildDate__c", date + "T10:00:00Z")
        }
        if(it.ZZDBM_V_VMSGEN.ZZ0015 != '0000-00-00') {
            def date = new Date().parse("yyyy-MM-dd", it.ZZDBM_V_VMSGEN.ZZ0015.text()).format("yyyy-MM-dd")
            listElement.put("OrderDate__c", date + "T10:00:00Z")
        }
        if(it.ZZDBM_V_VMSGEN.ZZ0028 != '0000-00-00') {
            def date = new Date().parse("yyyy-MM-dd", it.ZZDBM_V_VMSGEN.ZZ0028.text()).format("yyyy-MM-dd")
            listElement.put("DeliveryDate__c", date + "T10:00:00Z") //Fix: Integration account is GMT + 10 //remove this?
        }
        if(it.ZZDBM_V_RUS.ZZ0032 != '0000-00-00') {
            def date = new Date().parse("yyyy-MM-dd", it.ZZDBM_V_RUS.ZZ0032.text()).format("yyyy-MM-dd")
            listElement.put("Status6Date__c", date + "T10:00:00Z")
        }
        if(it.ZZDBM_V_RUS.ZZ0030 != '0000-00-00') {
            def date = new Date().parse("yyyy-MM-dd", it.ZZDBM_V_RUS.ZZ0030.text()).format("yyyy-MM-dd")
            listElement.put("RDADate__c", date + "T10:00:00Z")
        }  
        if(it.DBM_V_IPRICES.AIMPURPRI != '') {
            listElement.put("PurchasePrice__c", it.DBM_V_IPRICES.AIMPURPRI.text())
        }
        if(it.ZZDBM_V_GEN.ZZ0017 != '') {
            listElement.put("LatestValuation__c",it.ZZDBM_V_GEN.ZZ0017.text())
        } 
        if(it.DBM_V_IPRICES.ESTPURPRI != '') {
            listElement.put("TotalCost__c", it.DBM_V_IPRICES.ESTPURPRI.text())
        }
        if(it.DBM_V_IPRICES.ESTSALPRI != '') {
            listElement.put("Price__c", it.DBM_V_IPRICES.ESTSALPRI.text())
        }
        if(it.DBM_V_IVEHICLE.DIFFTAX != '') {
        listElement.put("PurchaseFromNonTaxRegVendor__c", boolMap[it.DBM_V_IVEHICLE.DIFFTAX.text()])
    }
    }
    //Owned specific mapping
    //NA
}

//If owned vehicles flow we need to map service history
    //add map of service history elements to be processed on the SF record
if(vehicleType == 'Owned') {
    ET_SRV_HIST.row.each {
        def vguid = it.VGUID.text()
        def listElement = vehicleList.find { it.VGUID == vguid}

        if(it.SRV_HIST != '') {
            it.SRV_HIST.row.each {
                
                def isLegacy = false
                if(it.LEGACY_DATA.text() == "X") {
                    isLegacy = true
                }
                
                srvHistInstance.put("legacyServiceHistory__c", isLegacy)
                
                srvHistInstance.put("Customer_Total_Amount__c", it.CUS_TOTAL.text())
                srvHistInstance.put("InternalTotalAmount__c", it.INT_TOTAL.text())
                srvHistInstance.put("Odometer__c", it.MILEAGE.text())
                if(it.FERT_DATE_TMSTP != '0') {
                    def roDate = new Date().parse("yyyyMMddHHmmss", it.FERT_DATE_TMSTP.text()).format("yyyy-MM-dd") //format 20160629093000
                    // srvHistInstance.put("ROCustomerPostDate__c", roDate + "T10:00:00Z")
                    srvHistInstance.put("ROPostDate__c", roDate + "T10:00:00Z")
                }
                srvHistInstance.put("RONumber__c", it.VBELN.text())                
                srvHistInstance.put("ROOpenDate__c", checkForEmptyDateField(it.AUDAT))

                srvHistInstance.put("ROTotalAmount__c", it.H_NETWR.text())
                srvHistInstance.put("WarrantyTotalAmount__c", it.WTY_TOTAL.text())

                srvHistList.push(srvHistInstance)
                srvHistInstance = [:]
            }
            listElement.put("SRV_HIST", srvHistList)
            srvHistList = []
        }
    }
}

//println(vehicleList)
message.payload = vehicleList



//helper functions
def checkForEmptyDateField(field) {
    if(field != '0000-00-00') {
        def date = new Date().parse("yyyy-MM-dd", field.text()).format("yyyy-MM-dd")
        return date + "T10:00:00Z" // dependant on timezone of integration account
    } 
    else {
        return ""
    }
}
def checkForEmptyStringField(field) {
    if(field != '') {
        return field.text()
    }
    else {
        return ''
    }
}


def convertStockMapping(primaryStatus, secondaryStatus, sector) {
    def value = sector.substring(1,4).toInteger()
    //println(primaryStatus)
    //println(secondaryStatus)
    //println(sector)
    if((primaryStatus == "ZP27" || primaryStatus == "ZP40" || primaryStatus == "ZP70" || primaryStatus == "ZP60") && secondaryStatus == "ZS05" && (value < 7 || value > 24)) {
        return "In Stock"
    }
    else if ((primaryStatus == "ZP27" || primaryStatus == "ZP40" || primaryStatus == "ZP60" || primaryStatus == "ZP70") && secondaryStatus == "ZS05" && value == 4) {
        return "Loan Car"
    }
    else if ((primaryStatus == "ZP20" || primaryStatus == "ZP30") && secondaryStatus == "ZS05" && (value < 7 || value > 24)) {
        return "On Order"
    }
    else if ((primaryStatus == "ZP27" || primaryStatus == "ZP40" || primaryStatus == "ZP60" || primaryStatus == "ZP70") && secondaryStatus == "ZS05" && value >= 7 && value <= 24 ) {
        return "Demo"
    } else {
        return null
    }
}