import groovy.xml.MarkupBuilder

import org.mule.streaming.ConsumerIterator;

def writer = new StringWriter()
def xml = new MarkupBuilder(writer)
def ConsumerIterator<?> i = (ConsumerIterator<?>) message.payload
def counter = 1
def salesTypes = [
	"Retail": "Z001",
	"Wholesale": "Z002",
	"Leasing": "Z003",
	"Corp. sale": "Z004",
	"Inchcape group": "Z005",
	"Promotion": "Z006",
	"Consigment": "Z007",
	"Finance": "Z008",
	"Buy back": "Z009",
	"Courtesy": "Z010",
	"Demo": "Z011",
	"Privilege card": "Z012",
	"Trade-In": "Z013",
	"Staff": "Z014",
	"Used Commission": "Z015",
	"Remote Sale": "Z016",
	"Internal Work": "Z017",
	"Insurance (Approved)": "Z018",
	"Insurance (Retail)": "Z019",
	"Government": "Z020"
]
def element = i.next()

xml.ZFM_GO_DBM_SAL_ORD_UPDATE { 
    'import' {
        IS_VBAK {
            AUDAT(element.Opportunity.StatusDate__c)
            VKBUR(salesTypes[element.Opportunity.Type])
            PERNR(element.Opportunity.Owner.SAP_Employee_Record_Number__c) //opportunity owner SAP ID
            KUNNR('0070000124') //business partner ID
            AUFART('ZV15') //constant
            VKORG('6106') //constant
            VTWEG('10') //constant
            SPART('SU') //constant
            WERKS('6106') //constant
            //ZTERM() //TBC
            CHGND('I')
        }
        IT_VBAP {
            while (true) {
                
                row {
                    POSNR(counter) //incremental
                    MAIN_ITEM(1)
                    if(element.Product2.Product_Material_Code__r != null) {
						MATNR40(element.Product2.Product_Material_Code__r.Name)  
						ITOBJID(element.Product2.Product_Material_Code__r.Name)           
                    }
					DESCR1(element.Description)
					if(element.Product2.Product_Item_Category_Code__r != null) {
						ITCAT(element.Product2.Product_Item_Category_Code__r.Name)
					}
					ZMENG(element.Quantity) //constant?
					KBETM(element.TotalPrice)
					//ZZMODEL_TAX_VAL('3090.91') //TBC
					OPCLASS(element.Product2.SAP_Options_Class__c)
					OPTYP(element.Product2.SAP_Option_Category__c)
					OPKEY(element.Product2.SAP_Option_Key__c)
					MCODESD(element.Product2.CarLine__c)
					REBATE(element.SBQQ__QuoteLine__r.SBQQ__AdditionalDiscountAmount__c)
					CHGND('I')
                    
                    //if Trade in add trade in fields to row.
                    if(element.SBQQ__QuoteLine__r.IsTradeIn__c == 'true') { 
                        if(element.SBQQ__QuoteLine__r.TradeIn__r != null) {
                            VERPR(element.TotalPrice)
                            ZZVHCLE(element.SBQQ__QuoteLine__r.TradeIn__r.VINAutocheck__c)                            
                        }
                    }
                }
                
                if(!i.hasNext()) {
                	break
                }
                counter++
                element = i.next()
            }
        }
        //for finance add IT_VBPA fields here
    }
}

message.payload = writer