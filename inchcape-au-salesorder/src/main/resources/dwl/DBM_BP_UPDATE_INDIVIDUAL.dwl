%dw 1.0
%output application/xml
%var ZPRIVACYCODES = {
	"PHONE / DIRECT MAIL": "AC",
	"EMAIL / PHONE / DIRECT MAIL": "EO",
	"EMAIL / PHONE / SMS / DIRECT M": "ES",
	"NO CAMPAIGNS / MARKETING": "NC",
	"SMS / PHONE / DIRECT MAIL": "SO"
}
%var COUNTRIES = {
	"Australia": "AU"
}
%var STATES = {
	"New South Wales": "NSW",
	"Victoria": "VIC",
	"Australian Capital Territory": "ACT",
	"Northern Territory": "NT",
	"Queensland": "QLD",
	"South Australia": "SA",
	"Tasmania": "TAS",
	"Western Australia": "WA"
}
%var SEXKEY = {
	"Male": "2",
	"Female": "1"
}
%var TITLEKEY = {
	"Ms.": "0001",
	"Mr.": "0002",
	"Miss": "Z001",
	"Sir": "Z002",
	"Lady": "Z003",
	"Prof.": "Z004",
	"Reverend": "Z005",
	"Mrs.": "Z006",
	"Dr.": "Z007",
	"Mr & Mrs": "Z008",
	"Mr & Ms": "Z009",
	"Mr & Miss": "Z010",
	"Mrs & Mrs": "Z011",
	"Miss & Miss": "Z012",
	"Ms & Ms": "Z013",
	"Mr & Mr": "Z014",
	"Capt": "Z015",
	"Commissioner": "Z016",
	"Father": "Z017",
	"Hon": "Z018",
	"Lord": "Z019",
	"Prince": "Z020",
	"Princess": "Z021",
	"Rabbi": "Z022",
	"Senator": "Z023",
	"Sister": "Z024"
}
---
{
	"ZFM_GO_DBM_BP_UPDATE": {
		import: {
			IS_ADDDATA: {
				ZCONTACT_TYP: "PRIVATE",
				ZPRIVACY_CODE: "ES"
			// <CAPRICUSTINDI>X</CAPRICUSTINDI> 
			// <CAPRICUSTMEMID>234567</CAPRICUSTMEMID> 
			},
			IS_CENTRAL: {
				// PARTNERTYPE: '2',
				PARTNEREXTERNAL: payload.LegacyID__c,
				TITLE_KEY: TITLEKEY[payload.Salutation],
				PARTNERLANGUAGE: "E"
			},
			IS_CENTRALPERS: {
				FIRSTNAME: payload.FirstName,
				LASTNAME: payload.LastName,
				MIDDLENAME: payload.MiddleName,
				BIRTHDATE: payload.Birthdate,
				CORRESPONDLANGUAGE: "E",
				SEX: SEXKEY[payload.Gender__c]
			},
			IS_ROLES: {
				//CUSTOMER: "X",
				//FICUSTOMER: "X",
				//INCHCAPE_PROSPECT: "X",
				//INCHCAPE_SOLDTO: "X",
				VENDOR: "X"
			},
			IT_ADDRESSES: {
				(row: {
					TYPE: {
						row: {
							ADR_TYPE: '0002' // residential
						}
					},
					ADDRESS: {
						CITY: payload.OtherCity,
						POSTL_COD1: payload.OtherPostalCode,
						STREET: payload.OtherStreet,
						HOUSE_NO: payload.ResidentialDPID__c,
						STR_SUPPL1: payload.OtherStreet,
						COUNTRY: COUNTRIES[payload.OtherCountry],
						REGION: STATES[payload.OtherState],
						LANGU: 'E'
					},
					ADTEL: {
						row: {
							TELEPHONE: payload.Phone,
							COMM_NOTES: 'Home'
						},
						row: {
							TELEPHONE: payload.WorkPhone__c,
							COMM_NOTES: 'Work'
						},
						row: {
							TELEPHONE: payload.MobilePhone,
							COMM_NOTES: 'Mobile'
						}
					},
					ADFAX: {
						row: {
							FAX: payload.Fax,
							COMM_NOTES: 'Fax'
						}
					},
					ADSMTP: {
						row: {
							E_MAIL: payload.BusinessEmail__c,
							COMM_NOTES: "Work"
						},
						row: {
							E_MAIL: payload.Email,
							COMM_NOTES: "Home"
						}
					}
				}) when (payload.OtherCity != null),
				(row: {
					TYPE: {
						row: {
							ADR_TYPE: '0001' // postal
						}
					},
					ADDRESS: {
						CITY: payload.MailingCity,
						POSTL_COD1: payload.MailingPostalCode,
						STREET: payload.MailingStreet,
						HOUSE_NO: payload.MailingDPID__c,
						COUNTRY: COUNTRIES[payload.MailingCountry],
						REGION: STATES[payload.MailingState],
						LANGU: 'E'
					},
					ADTEL: {
						row: {
							TELEPHONE: payload.Phone,
							COMM_NOTES: 'Home'
						},
						row: {
							TELEPHONE: payload.WorkPhone__c,
							COMM_NOTES: 'Work'
						},
						row: {
							TELEPHONE: payload.MobilePhone,
							COMM_NOTES: 'Mobile'
						}
					},
					ADFAX: {
						row: {
							FAX: payload.Fax,
							COMM_NOTES: 'Fax'
						}
					},
					ADSMTP: {
						row: {
							E_MAIL: payload.BusinessEmail__c,
							COMM_NOTES: "Work"
						},
						row: {
							E_MAIL: payload.Email,
							COMM_NOTES: "Home"
						}
					}
				}) when (payload.MailingCity != null)
			},
			IV_PARTNER: payload.CustomerID__c,
			IV_IDNUMBER: payload.Id,
			IV_TYPE: "ZSF"
		}
	}
}