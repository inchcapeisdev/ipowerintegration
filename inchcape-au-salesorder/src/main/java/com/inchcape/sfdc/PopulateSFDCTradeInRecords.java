package com.inchcape.sfdc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mule.api.transformer.TransformerException;
import org.mule.streaming.ConsumerIterator;
import org.mule.transformer.AbstractTransformer;

public class PopulateSFDCTradeInRecords extends AbstractTransformer {
	
	@SuppressWarnings("unchecked")
	@Override
	protected Object doTransform(Object src, String enc) throws TransformerException {
		
		ConsumerIterator<?> i = (ConsumerIterator<?>) src;
		Map<String, Object> elements = null;
		
		List<Map<String, Object>> tradeInResultsList = new ArrayList <Map<String, Object>>();
		Map<String, Object> tradeInInstance = new HashMap<String, Object>();
		Map<String, Object> tradeIn__r = null;
		Map<String, Object> sBQQ__QuoteLine__r = null;
		
		while (i.hasNext())
		{
			tradeInInstance = new HashMap<String, Object>();
			elements = (HashMap<String, Object>) i.next();
			
			sBQQ__QuoteLine__r = (HashMap<String, Object>)elements.get("SBQQ__QuoteLine__r");
			
			if (sBQQ__QuoteLine__r != null)
				tradeIn__r = (HashMap<String, Object>)sBQQ__QuoteLine__r.get("TradeIn__r");
			
			
			if (tradeIn__r != null)
			{
				//System.out.println("--------------------VIN Number:" + String.valueOf(tradeIn__r.get("VINAutocheck__c")));
				tradeInInstance.put("VendorId", String.valueOf(tradeIn__r.get("Account__c")) );
				tradeInInstance.put("VINAutocheck__c", String.valueOf(tradeIn__r.get("VINAutocheck__c")) );
				tradeInInstance.put("LatestValuationAmount__c", String.valueOf(tradeIn__r.get("LatestValuationAmount__c")) );
				tradeInInstance.put("RegistrationAutocheck__c", String.valueOf(tradeIn__r.get("RegistrationAutocheck__c")) );
				tradeInInstance.put("RegistrationExpiryAutocheck__c", String.valueOf(tradeIn__r.get("RegistrationExpiryAutocheck__c")) );
				tradeInInstance.put("EngineNumberAutocheck__c", String.valueOf(tradeIn__r.get("EngineNumberAutocheck__c")) );
				tradeInInstance.put("ComplianceDateAutocheck__c", String.valueOf(tradeIn__r.get("ComplianceDateAutocheck__c")) );
				
				tradeInInstance.put("BuildDateAutocheck__c", String.valueOf(tradeIn__r.get("BuildDateAutocheck__c")) );
				tradeInInstance.put("OdometerAutocheck__c", String.valueOf(tradeIn__r.get("OdometerAutocheck__c")) );
				tradeInInstance.put("LatestAppraisalDateTime__c", String.valueOf(tradeIn__r.get("LatestAppraisalDateTime__c")) );
				tradeInInstance.put("ContractValuationAmount__c", String.valueOf(tradeIn__r.get("ContractValuationAmount__c")) );
				tradeInInstance.put("ValuationAmountApproved__c", String.valueOf(tradeIn__r.get("ValuationAmountApproved__c")) );
				tradeInInstance.put("MakeModelAutocheck__c", String.valueOf(tradeIn__r.get("MakeModelAutocheck__c")) );
			}
			
			
//			if (sBQQ__Quote__r != null)
//			{
//				tradeInInstance.put("SBQQ_Account__c", String.valueOf(sBQQ__Quote__r.get("SBQQ__Account__c")) );
//				
//				if ( (HashMap<String, Object>)sBQQ__Quote__r.get("SBQQ__Account__r") != null)
//				{
//					tradeInInstance.put("SBQQ_Account__c_Name", String.valueOf(((HashMap<String, Object>)sBQQ__Quote__r.get("SBQQ__Account__r")).get("Name")) );
//					tradeInInstance.put("SBQQ_Account__r_OrganisationID", String.valueOf(((HashMap<String, Object>)sBQQ__Quote__r.get("SBQQ__Account__r")).get("OrganisationID__c")) );
//					
//					if ( ((HashMap<String, Object>)((HashMap<String, Object>)sBQQ__Quote__r.get("SBQQ__Account__r")).get("npe01__One2OneContact__r")) != null)
//					{
//						tradeInInstance.put("SBQQ_Account_One2OneContact_CustomerId", String.valueOf( ((HashMap<String, Object>)((HashMap<String, Object>)sBQQ__Quote__r.get("SBQQ__Account__r")).get("npe01__One2OneContact__r")).get("CustomerId__c") ) );
//					}
//				}
//				
//				if ( (HashMap<String, Object>)sBQQ__Quote__r.get("SBQQ__Opportunity2__r") != null)
//				{
//					tradeInInstance.put("SBQQ_Opportunity_StatusDate", String.valueOf(((HashMap<String, Object>)sBQQ__Quote__r.get("SBQQ__Opportunity2__r")).get("StatusDate__c")) );
//					tradeInInstance.put("SBQQ_OpportunityType", String.valueOf(((HashMap<String, Object>)sBQQ__Quote__r.get("SBQQ__Opportunity2__r")).get("Type")) );
//				}
//			}
			tradeInResultsList.add(tradeInInstance);
		}
	
		return tradeInResultsList; 
	}
}
