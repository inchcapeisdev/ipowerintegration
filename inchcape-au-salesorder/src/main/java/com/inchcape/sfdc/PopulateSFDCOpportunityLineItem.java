package com.inchcape.sfdc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mule.api.transformer.TransformerException;
import org.mule.streaming.ConsumerIterator;
import org.mule.transformer.AbstractTransformer;

public class PopulateSFDCOpportunityLineItem extends AbstractTransformer {
	
	@SuppressWarnings("unchecked")
	@Override
protected Object doTransform(Object src, String enc) throws TransformerException {
        
        ConsumerIterator<?> i = (ConsumerIterator<?>) src;
        Map<String, Object> elements = null;
        
        List<Map<String, Object>> opportunityLineItems = new ArrayList <Map<String, Object>>();
        Map<String, Object> opportunityLineItemInstance = new HashMap<String, Object>();
        Map<String, Object> account = null;
        Map<String, Object> opportunity = null;
        
        while (i.hasNext())
        {
            elements = (HashMap<String, Object>) i.next();
            //opportunityLineItemInstance = new HashMap<String, Object>();
            opportunity = (HashMap<String, Object>)elements.get("Opportunity");
            
            if (opportunity != null)
            {
                account = (HashMap<String, Object>)opportunity.get("Account");
                opportunityLineItemInstance.put("StatusDate", String.valueOf(opportunity.get("StatusDate__c")) );
                opportunityLineItemInstance.put("Type", String.valueOf(opportunity.get("Type")) );
            }
            
            if (account != null)
            {
                opportunityLineItemInstance.put("ID", String.valueOf(account.get("ID")) );
                opportunityLineItemInstance.put("Name", String.valueOf(account.get("Name")) );
                
                if ( account.get("npe01__One2OneContact__r") != null )
                {
                    opportunityLineItemInstance.put("CustomerId", String.valueOf( ((HashMap<String, Object>)account.get("npe01__One2OneContact__r")).get("CustomerID__c") ) );
                }
            }

            opportunityLineItems.add(opportunityLineItemInstance);
            opportunityLineItemInstance = new HashMap<String, Object>();
        }
    
        return opportunityLineItems; 
    }
}
