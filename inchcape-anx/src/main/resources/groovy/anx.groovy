import groovy.util.XmlSlurper
import groovy.xml.MarkupBuilder
import java.util.Date

def writer = new StringWriter()
def xml = new MarkupBuilder(writer)

def ZVSG_AUTO_NEXUS_PO_GET = new XmlSlurper().parseText(message.payload) 
def ET_PO_HEADER = ZVSG_AUTO_NEXUS_PO_GET.export.ET_PO_HEADER
def ET_PO_ITEMS = ZVSG_AUTO_NEXUS_PO_GET.export.ET_PO_ITEMS
def ET_PO_JOBS = ZVSG_AUTO_NEXUS_PO_GET.export.ET_PO_JOBS

def orderList = []
def itemList = []
def xmlOutput = []
def xmlElement = ""
def orderInstance = [:]
def itemInstance = [:]
def jobInstance = [:]


ET_PO_HEADER.row.each { 
    orderInstance.put("SITEID", it.SITEID.text())
    orderInstance.put("ORDERNUMBER", it.ORDERNUMBER.text())
    orderInstance.put("RONUMBER", it.RONUMBER.text())
    orderInstance.put("ROCOMMENTS", it.ROCOMMENTS.text())
    orderInstance.put("ADVISORNUMBER", it.ADVISORNUMBER.text())
    orderInstance.put("ADVISORNAME", it.ADVISORNAME.text())
    orderInstance.put("VIN", it.VIN.text())
    orderInstance.put("VEHICLE_REG", it.VEHICLE_REG.text())
    orderInstance.put("BRAND", it.BRAND.text())
    orderInstance.put("MATERIAL", it.MATERIAL.text())
    orderInstance.put("MATERIAL_DESCR", it.MATERIAL_DESCR.text())
    orderInstance.put("LOCATION_CODE", it.LOCATION_CODE.text())
    orderInstance.put("STOCK_NO", it.STOCK_NO.text())
    orderInstance.put("SOLD_TO_CUST", it.SOLD_TO_CUST.text())
    orderInstance.put("SOLD_TO_NAME", it.SOLD_TO_NAME.text())
    orderInstance.put("SOLD_TO_STREET", it.SOLD_TO_STREET.text())
    orderInstance.put("SOLD_TO_CITY", it.SOLD_TO_CITY.text())
    orderInstance.put("SOLD_TO_POSTCODE", it.SOLD_TO_POSTCODE.text())
    orderInstance.put("SOLD_TO_COUNTRY", it.SOLD_TO_COUNTRY.text())
    orderInstance.put("SOLD_TO_TELEPHONE", it.SOLD_TO_TELEPHONE.text())
    if( it.DEALER_DELIV_DATE != '' ) {
        Date DEALER_DELIV_DATE = Date.parse( 'YYYYMMdd', it.DEALER_DELIV_DATE.text() )
        orderInstance.put("DEALER_DELIV_DATE", DEALER_DELIV_DATE.format("ddMMYYYY")) //ddMMYYYY
    } else {
        orderInstance.put("DEALER_DELIV_DATE", '') //ddMMYYYY
    }
    orderInstance.put("DEALER_DELIV_TIME", it.DEALER_DELIV_TIME.text())
    if( it.CUSTMOER_DELIV_DATE != '' ) {
        Date CUSTMOER_DELIV_DATE = Date.parse( 'YYYYMMdd', it.CUSTMOER_DELIV_DATE.text() )
        orderInstance.put("CUSTMOER_DELIV_DATE", CUSTMOER_DELIV_DATE.format("ddMMYYYY")) //ddMMYYYY
    } else {
        orderInstance.put("CUSTMOER_DELIV_DATE", '') //ddMMYYYY
    }
    orderInstance.put("CUSTOMER_DELIV_TIME", it.CUSTOMER_DELIV_TIME.text())
    orderInstance.put("CANCELLED_IND", it.CANCELLED_IND.text())
    orderInstance.put("ITEMS", [])
    orderInstance.put("JOBS", [])
    
    orderList.push(orderInstance)
    orderInstance = [:]
}

ET_PO_ITEMS.row.each {
    def ORDERNUMBER = it.ORDERNUMBER.text()
    def orderElement = orderList.find { it.ORDERNUMBER == ORDERNUMBER }
    
    itemInstance.put("JOB_NUMBER", it.JOB_NUMBER.text())
    itemInstance.put("ITEM_TYPE", it.ITEM_TYPE.text())
    itemInstance.put("ITEM_CODE", it.ITEM_CODE.text())
    itemInstance.put("ITEM_DESC", it.ITEM_DESC.text())
    itemInstance.put("ITEM_QTY", it.ITEM_QTY.text())
    itemInstance.put("ITEM_UOM", it.ITEM_UOM.text())
    itemInstance.put("SALE_PRICE", it.SALE_PRICE.text())
    itemInstance.put("COST_PRICE", it.COST_PRICE.text())
    itemInstance.put("CURRENCY", it.CURRENCY.text())
    itemInstance.put("COMMENTS", it.COMMENTS.text())
    
    //orderElement.put("ITEMS",itemInstance)
    orderElement.ITEMS << itemInstance
    itemInstance = [:]
}
    
ET_PO_JOBS.row.each {
    def ORDERNUMBER = it.ORDERNUMBER.text()
    def orderElement = orderList.find { it.ORDERNUMBER == ORDERNUMBER }
    
    jobInstance.put("JOB_NUMBER", it.JOB_NUMBER.text())
    jobInstance.put("OPERATION_CODE", it.OPERATION_CODE.text())
    jobInstance.put("OPERATION_DESC", it.OPERATION_DESC.text())
    jobInstance.put("OPERATION_PRICE", it.OPERATION_PRICE.text())
    jobInstance.put("CURRENCY", it.CURRENCY.text())
    jobInstance.put("CANCELLED", it.CANCELLED.text())
    
    //orderElement.put("JOBS",jobInstance)
    orderElement.JOBS << jobInstance
    jobInstance = [:]
}

//orderList.each {
//    it.remove('ORDERNUMBER')
//}



//GENERATE XML
orderList.each { order -> 
    xml.ZANX_VSG_PO_RECEIVER() {    
        'import' {
                I_HEADER {
                    SITEID(order.SITEID)
                    ORDERNUMBER(order.ORDERNUMBER)
                    RONUMBER(order.RONUMBER)
                    ROCOMMENTS(order.ROCOMMENTS.trim())
                    ADVISORNUMBER(order.ADVISORNUMBER.trim())
                    ADVISORNAME(order.ADVISORNAME.trim())
                    VIN(order.VIN)
                    VEHICLE_REG(order.VEHICLE_REG.trim())
                    BRAND(order.BRAND.trim())
                    MATERIAL(order.MATERIAL.trim())
                    MATERIAL_DESCR(order.MATERIAL_DESCR.trim())
                    LOCATION_CODE(order.LOCATION_CODE.trim())
                    STOCK_NO(order.STOCK_NO.trim())
                    SOLD_TO_CUST(order.SOLD_TO_CUST.trim())
                    SOLD_TO_NAME(order.SOLD_TO_NAME.trim())
                    SOLD_TO_STREET(order.SOLD_TO_STREET.trim())
                    SOLD_TO_CITY(order.SOLD_TO_CITY.trim())
                    SOLD_TO_POSTCODE(order.SOLD_TO_POSTCODE.trim())
                    SOLD_TO_COUNTRY(order.SOLD_TO_COUNTRY.trim())
                    SOLD_TO_TELEPHONE(order.SOLD_TO_TELEPHONE.trim())
                    DEALER_DELIV_TIME(order.DEALER_DELIV_TIME)
                    CUSTMOER_DELIV_DATE(order.CUSTMOER_DELIV_DATE)
                    CUSTOMER_DELIV_TIME(order.CUSTOMER_DELIV_TIME)
                    CANCELLED_IND(order.CANCELLED_IND.trim())
                }
            I_ITEMS {
                order.ITEMS.each { item ->
                    //item*.trim()
                    row {
                        JOB_NUMBER(item.JOB_NUMBER)
                        ITEM_TYPE(item.ITEM_TYPE.trim())
                        ITEM_CODE(item.ITEM_CODE.trim())
                        ITEM_DESC(item.ITEM_DESC.trim())
                        ITEM_QTY(item.ITEM_QTY.trim())
                        ITEM_UOM(item.ITEM_UOM.trim())
                        SALE_PRICE(item.SALE_PRICE.trim())
                        COST_PRICE(item.COST_PRICE.trim())
                        CURRENCY(item.CURRENCY.trim())
                        COMMENTS(item.COMMENTS.trim())
                    }
                }
            }
            I_JOBS {
                order.JOBS.each { job ->
                    row {
                        JOB_NUMBER(job.JOB_NUMBER)
                        OPERATION_CODE(job.OPERATION_CODE.trim())
                        OPERATION_DESC(job.OPERATION_DESC.trim())
                        OPERATION_PRICE(job.OPERATION_PRICE.trim())
                        CURRENCY(job.CURRENCY.trim())
                        CANCELLED(job.CANCELLED.trim())
                    }
                }
            }
        }
    }
    xmlElement = writer.toString()
    xmlOutput << xmlElement
    xmlElement = ""
    writer.getBuffer().setLength(0)
}

//log.info(xmlOutput)

message.payload = xmlOutput